#!/usr/bin/env python3

from elg import FlaskService
from elg.model import AnnotationsResponse

from estnltk import Text
from estnltk.taggers import SentenceTokenizer
from typing import Dict

'''
# command line script
python3 -m venv venv_elg_estnltk
venv_elg_estnltk/bin/python3 -m pip install --upgrade pip
venv_elg_estnltk/bin/pip3 --no-cache-dir install -r requirements.txt
venv_elg_estnltk/bin/python3 ./elg_sdk_tokenizer.py --json='{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad"}'

# web server in docker & curl
venv_elg_estnltk/bin/elg docker create --path elg_sdk_tokenizer.py --classname EstNLTK_tokenizer --requirements estnltk==1.6.9.1b0 --requirements requests --base_image python:3.8-slim-bullseye
# in docker-entrypoint.sh change port to 6000
docker build -t tilluteenused/estnltk_tokenizer .
docker run -p 6000:6000 tilluteenused/estnltk_tokenizer
curl -i --request POST --header "Content-Type: application/json" --data '{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad"}' localhost:6000/process
'''


class EstNLTK_tokenizer(FlaskService):
    def process_text(self, content) -> AnnotationsResponse:
        '''
        Find sentences and tokens
        :param content: {TextRequest} - input text in ELG-format
        :return: {AnnotationsResponse} - sentence and token boundaries in ELG-annotation format
        '''
        return AnnotationsResponse(annotations=self.estnltk_lausesta_text(content.content))

    def estnltk_lausesta_text(self, text_in: str) -> Dict:
        '''
        Find sentences and tokens
        :param text_in: input text
        :return: sentence and token boundaries in ELG-annotation format
        '''
        estnltk_text = Text(text_in)
        estnltk_text.tag_layer(['words'])
        SentenceTokenizer().tag(estnltk_text)
        sentences = []
        tokens = []
        for sentence in estnltk_text.sentences:
            for word in sentence:
                tokens.append({"start": word.start, "end": word.end, "features": {"token": word.enclosing_text}})
            sentences.append({"start": sentence.start, "end": sentence.end})
            assert(tokens[0]["start"] == sentences[0]["start"])
            assert(tokens[-1]["end"] == sentences[-1]["end"])
        return {"sentence": sentences, "token": tokens}

flask_service = EstNLTK_tokenizer("EstNLTK tokenizer")
app = flask_service.app


def run_test(my_query_str: str) -> Dict:
    '''
    Run as command line script
    :param my_query_str: input in json string
    '''
    from elg.model import TextRequest
    my_query = json.loads(my_query_str)
    service = EstNLTK_tokenizer("EstNLTK tokenizer")
    request = TextRequest(content=my_query["content"])
    response = service.process_text(request)

    response_json_str = response.json(exclude_unset=True)  # exclude_none=True?
    response_json_json = json.loads(response_json_str)
    return response_json_json


def run_server() -> None:
    '''
    Run as flask webserver
    '''
    app.run()


if __name__ == '__main__':
    import argparse
    import json
    import sys
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='ELG compatible json')
    args = argparser.parse_args()
    if args.json is None:
        run_server()
    else:
        json.dump(run_test(args.json), sys.stdout, indent=4)
