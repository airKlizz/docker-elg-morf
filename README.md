## Container with a morphological analyzer for Estonian 
[Vabamorf](https://github.com/Filosoft/vabamorf) container (docker) with 
interface compliant with [ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Preliminaries
* Install version control software (if you download the software from the command line), as instructed [on the git website](https://git-scm.com/)
* Install software for making / using the container, as instructed on the [docker website](https://docs.docker.com/)

## Downloading the source code
Download from [GitLab web environment](https://gitlab.com/tilluteenused/docker-elg-morf.git), or use Linux command line (Windows / Mac command lines are similar):

```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker-elg-morf.git gitlab-docker-elg-morf
```
## Building the container
Linux command line (Windows / Mac command lines are similar):
```commandline
cd ~/gitlab-docker-elg/gitlab-docker-elg-morf
docker build -t tilluteenused/vabamorf_morf .
```
## Download image from Docker Hub
Linux command line (Windows / Mac command lines are similar):
```commandline
docker pull tilluteenused/vabamorf_morf
```

## Starting the container
Linux command line (Windows / Mac command lines are similar):
```commandline
docker run -p 7000:7000 tilluteenused/vabamorf_morf
```
One need not be in a specific directory to start the container.

Ctrl + C in a terminal window with a running container in it terminates it.

## Usage example
```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad", "annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees"}},{"start":5,"end":10,"features":{"token":"peeti"}},{"start":11,"end":16,"features":{"token":"kinni"}},{"start":16,"end":17,"features":{"token":"."}},{"start":18,"end":24,"features":{"token":"Sarved"}},{"start":24,"end":25,"features":{"token":"&"}},{"start":25,"end":30,"features":{"token":"S\u00f5rad"}}]}}' localhost:7000/process

HTTP/1.1 200 OK
Server: gunicorn
Date: Sat, 05 Feb 2022 09:59:46 GMT
Connection: close
Content-Type: application/json
Content-Length: 1418

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees","morph":[{"lemma":"Mee+s","pos":"H","feature":"sg in"},{"lemma":"Mees+0","pos":"H","feature":"sg n"},{"lemma":"Mesi+s","pos":"H","feature":"sg in"},{"lemma":"mees+0","pos":"S","feature":"sg n"},{"lemma":"mesi+s","pos":"S","feature":"sg in"}]}},{"start":5,"end":10,"features":{"token":"peeti","morph":[{"lemma":"peet+0","pos":"S","feature":"adt"},{"lemma":"pida+ti","pos":"V","feature":"ti"},{"lemma":"peet+0","pos":"S","feature":"sg p"}]}},{"start":11,"end":16,"features":{"token":"kinni","morph":[{"lemma":"kinni+0","pos":"D","feature":""}]}},{"start":16,"end":17,"features":{"token":".","morph":[{"lemma":".","pos":"Z","feature":""}]}},{"start":18,"end":24,"features":{"token":"Sarved","morph":[{"lemma":"Sarv+d","pos":"H","feature":"pl n"},{"lemma":"Sarve+d","pos":"H","feature":"pl n"},{"lemma":"Sarved+0","pos":"H","feature":"sg n"},{"lemma":"sarv+d","pos":"S","feature":"pl n"}]}},{"start":24,"end":25,"features":{"token":"&","morph":[{"lemma":"&+0","pos":"J","feature":""}]}},{"start":25,"end":30,"features":{"token":"S\u00f5rad","morph":[{"lemma":"S\u00f5ra+d","pos":"H","feature":"pl n"},{"lemma":"S\u00f5rad+0","pos":"H","feature":"sg n"},{"lemma":"S\u00f5rg+d","pos":"H","feature":"pl n"},{"lemma":"s\u00f5rg+d","pos":"S","feature":"pl n"}]}}]}}}
```
Note that the Python json library renders text in ASCII by default;
accented letters etc. are presented as Unicode codes, e.g. õ = \u00f5.

## Query json
```json
{
  "type": "text",
  "content": string, /* "The text of the request" */
  "annotations": {
    "sentence": [        /* array of sentences */
      {
        "start": 0,      /* beginning of sentence */
        "end": 17        /* end of sentence */
      }
    ],
    "token": [           /* all tokens across all sentences */
      {
        "start": number, /* beginning of token */
        "end": number,   /* end of token */
        "features": {
          "token": string /* token */
        }
      }
    ]
  }
}
```
The query should be tokenized and presented as sentences. One can use the [tokenizer container](https://gitlab.com/tarmo.vaino/docker-elg-tokenizer) for it.

## Response json
```json
{
  "response":
  {
    "type":"annotations",
    "annotations": {
      "sentence": [        /* array of sentences */
        {
          "start": 0,      /* beginning of sentence */
          "end": 17        /* end of sentence */
        }
      ],
      "token": [           /* all tokens across all sentences */
        {
          "start": number, /* beginning of token */
          "end": number,   /* end of token */
          "features": {
            "token": string /* token */
            "morph": [
              {
                "lemma":string,    /* lemma */
                "pos":string,      /* part of speech */
                "features":string  /* case etc */
              }
            ]            
          }
        }
      ]
    }
  }
}
```
The response follows the [Filosoft conventions](https://filosoft.ee/html_morf_et/morfoutinfo.html).

*NB! The alternative analyses are not ordered by likelihood!*

## Authors
Tarmo Vaino, Heiki-Jaan Kaalep

## Licence
Copyright (c) 2021 University of Tartu and Author(s).
Software is licensed under LGPL.
