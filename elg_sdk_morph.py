#!/usr/bin/env python3

from subprocess import Popen, PIPE, STDOUT
from typing import Dict, List

from elg import FlaskService
from elg.model import AnnotationsResponse

from elg_sdk_tokenizer import EstNLTK_tokenizer

# from inspect import currentframe, getframeinfo
# print(getframeinfo(currentframe()).filename, getframeinfo(currentframe()).lineno)

'''
# command line script
python3 -m venv venv_elg_vabamorf
venv_elg_vabamorf/bin/python3 -m pip install --upgrade pip
venv_elg_vabamorf/bin/pip3 --no-cache-dir install -r requirements.txt
venv_elg_vabamorf/bin/python3 ./elg_sdk_morph.py --json='{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad", "annotations": {"sentences": [{"start": 0, "end": 17, "features": {"tokens": [{"start": 0, "end": 4, "token": "Mees"}, {"start": 5, "end": 10, "token": "peeti"}, {"start": 11, "end": 16, "token": "kinni"}, {"start": 16, "end": 17, "token": "."}]}}, {"start": 18, "end": 30, "features": {"tokens": [{"start": 18, "end": 24, "token": "Sarved"}, {"start": 24, "end": 25, "token": "&"}, {"start": 25, "end": 30, "token": "S\u00f5rad"}]}}]}}'

# web server in docker & curl
venv_elg_vabamorf/bin/elg docker create --path elg_sdk_morph.py --classname FiloSoft_morph --required_files vmeta --required_files et.dct --requirements requests 
# in docker-entrypoint.sh change port to 7000
docker build -t tilluteenused/vabamorf_morf .
docker run -p 7000:7000 tilluteenused/vabamorf_morf
curl -i --request POST --header "Content-Type: application/json" --data '{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad", "annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees"}},{"start":5,"end":10,"features":{"token":"peeti"}},{"start":11,"end":16,"features":{"token":"kinni"}},{"start":16,"end":17,"features":{"token":"."}},{"start":18,"end":24,"features":{"token":"Sarved"}},{"start":24,"end":25,"features":{"token":"&"}},{"start":25,"end":30,"features":{"token":"S\u00f5rad"}}]}}' localhost:7000/process
'''


class FS_IO_CONV:
    def elg_sentences_tokens_2_fs_xml_text(self, sentences: List, tokens: List) -> str:
        '''
        elg sõnestaja väljund morfi sisendstringiks
        @param sentences: lausete massiiv
        @param tokens: sõnede massiiv
        @return: <s> token1 token2 ... </s>\n...
        '''
        text = ''
        s = t = 0
        while s < len(sentences):
            assert(tokens[t].start == sentences[s].start)
            text += '<s>'
            while t < len(tokens) and tokens[t].end <= sentences[s].end:
                text += f' {self.text_2_xml(tokens[t].features["token"])}'
                t += 1
            text += ' </s>\n'
            s += 1
        return text

    def fs_xml_morph_2_elg_morph(self, mrfstr: str) -> Dict:
        '''
        morfi väljundstring elg-kõlbulikuks
        :param mrfstr: TEKSTISÕNE   LEMMA //_POS_ FEATURES, //[...    LEMMA //_POS_ FEATURES, //]
        :return: { "token":string, "morph": [{ "lemma":string, "pos": string, "features":string }, ...] }
        '''
        eof_wordform = mrfstr.find('    ')
        startpos = eof_wordform+4
        analyysid_lst = []
        # tsükkel üle analüüsivariantide
        while startpos < len(mrfstr):
            endpos = mrfstr.find('    ', startpos)
            if endpos == -1:
                endpos = len(mrfstr)
            analyysid_lst.append(self.morph1str_2_dct(mrfstr[startpos:endpos]))
            startpos = endpos+4

        wordform = self.xml_2_txt(mrfstr[:eof_wordform])
        return {"token": wordform, "morph": analyysid_lst}

    def text_2_xml(self, s: str) -> str:
        '''
        Teisendab: plain-text -> fs-xml
        @param s: sisse plain-text
        @return: tagasi fs-xml
        '''
        return s.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace(' ', '<space/>')

    def xml_2_txt(self, s: str) -> str:
        '''
        Teisendab: fs-xml kuju -> plain-text
        @param s: sisse fs-xml
        @return: tagasi plain-text
        '''
        return s.replace('<space/>', ' ').replace('&amp;', '&').replace('&lt;', '<').replace('&gt;', '>')

    def morph1str_2_dct(self, analyys_str: str) -> Dict:
        '''
        FS analüüs -> ELG DCT
        :param analyys_str: sisse "LEMMA //_POS_ FEATURES, //"
        :return: { "lemma":string, "pos": string, "feature":string }
        '''
        analyys_dct = {}
        eof_lemma = analyys_str.find(' ')
        analyys_dct["lemma"] = analyys_str[:eof_lemma]
        analyys_dct["pos"] = analyys_str[eof_lemma+4]
        if analyys_str[eof_lemma+7] == '/':
            analyys_dct["feature"] = ''  # vormi pole
        else:
            analyys_dct["feature"] = analyys_str[eof_lemma+7:len(analyys_str)-3]
            if analyys_dct["feature"][len(analyys_dct["feature"])-1] == ',':
                analyys_dct["feature"] = analyys_dct["feature"][:len(analyys_dct["feature"])-1]
        return analyys_dct


class FiloSoft_morph(FlaskService):

    tokenizer = EstNLTK_tokenizer("EstNLTK tokenizer")

    def process_text(self, request):

        if request.annotations == None or not ("sentence" in request.annotations.keys() and "token" in request.annotations.keys()):
            tok_resp = self.tokenizer.process_text(content=request)
            request.annotations = tok_resp.annotations

        sentences = request.annotations["sentence"]
        tokens_in = request.annotations["token"]
        tokens_out = self.filosoft_morph(sentences, tokens_in)
        annotation_out = {'sentence': sentences, "token": tokens_out}
        anno_resp = AnnotationsResponse(annotations=annotation_out)
        return anno_resp

    def filosoft_morph(self, sentences: List, tokens: List) -> List:
        conv = FS_IO_CONV()
        text = conv.elg_sentences_tokens_2_fs_xml_text(sentences, tokens)
        p = Popen(['./vmeta', '--xml', '--path', '.'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
        lines = p.communicate(input=bytes(text, 'utf-8'))[0].decode('utf-8').split('\n')
        # tükeldame morf analüüsi tulemuse püütoni sõnastikuks ja
        # lisame analüüsid algsele andmestikule
        l = s = t = 0
        while s < len(sentences):
            assert(lines[l] == '<s>')
            l += 1
            while l < len(lines) and t < len(tokens) and tokens[t].end <= sentences[s].end:
                m = conv.fs_xml_morph_2_elg_morph(lines[l])
                assert(m["token"] == tokens[t].features["token"])
                tokens[t].features = m
                t += 1
                l += 1
            assert(lines[l] == '</s>')
            l += 1
            s += 1
        return tokens


flask_service = FiloSoft_morph("Filosofti morfoloogiline analüsaator")
app = flask_service.app


def run_test(my_query_str: str) -> None:
    '''
    Run as command line script
    :param my_query_str: input in json string
    '''
    from elg.model import TextRequest
    my_query = json.loads(my_query_str)
    service = FiloSoft_morph("Filosofti morfoloogiline analüsaator")
    request = TextRequest(content=my_query["content"], annotations=my_query["annotations"])
    response = service.process_text(request)

    response_json_str = response.json(exclude_unset=True)  # exclude_none=True
    response_json_json = json.loads(response_json_str)
    return response_json_json


def run_server():
    '''
    Run as flask webserver
    '''
    app.run()


if __name__ == '__main__':
    import json
    import sys
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='ELG compatible json')
    args = argparser.parse_args()
    if args.json is None:
        run_server()
    else:
        '''
        query_json = \
            {
                "type": "text",
                "content": "Mees peeti kinni. Sarved&S\u00f5rad",
                "annotations": {
                    "sentence": [
                        {"start": 0, "end": 17},
                        {"start": 18, "end": 30}
                    ],
                    "token": [
                        {"start": 0, "end": 4, "features": {"token": "Mees"}},
                        {"start": 5, "end": 10, "features": {"token": "peeti"}},
                        {"start": 11, "end": 16, "features": {"token": "kinni"}},
                        {"start": 16, "end": 17, "features": {"token": "."}},
                        {"start": 18, "end": 24, "features": {"token": "Sarved"}},
                        {"start": 24, "end": 25, "features": {"token": "&"}},
                        {"start": 25, "end": 30, "features": {"token": "S\u00f5rad"}}
                    ]
                }
            }
        query_text=json.dumps(query_json)
        json.dump(run_test(query_text), sys.stdout, indent=4)
        '''
        json.dump(run_test(args.json), sys.stdout, indent=4)
