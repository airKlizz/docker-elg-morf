## Eesti keele morfoloogilise analüsaatori konteiner
[Vabamorfi](https://github.com/Filosoft/vabamorf) sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Eeltöö
* Paigalda versioonihaldustarkvara (kui laadid tarkvara all käsurealt), juhised [git'i veebilehel](https://git-scm.com/)
* Paigalda tarkvara konteineri tegemiseks/kasutamiseks, juhised [docker'i veebilehel](https://docs.docker.com/)

## Lähtekoodi allalaadimine
Allalaadimiseks võib kasutada [GitLab'i veebikeskkonda](https://gitlab.com/tilluteenused/docker-elg-morf.git).

Linux'i käsurealt lähtekoodi allalaadimine (Windows'i/Mac'i käsurida on analoogiline):
```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone  https://gitlab.com/tilluteenused/docker-elg-morf.git gitlab-docker-elg-morf
```
## Konteineri ehitamine
Linux'i käsurealt konteineri ehitamine (Windows'i/Mac'i käsurida on analoogiline)
```commandline
cd ~/gitlab-docker-elg/gitlab-docker-elg-morf
docker build -t tilluteenused/vabamorf_morf .
```

## Konteineri allalaadimine Docker Hub'ist
Allalaaditud lähtekoodist konteineri ehitamise asemel võib valmis konteineri allalaadida Docker Hub'ist.

Linux'i käsurealt konteineri allalaadimine (Windows'i/Mac'i käsurida on analoogiline)
```commandline
docker pull tilluteenused/vabamorf_morf
```

## Konteineri käivitamine
Linux'i käsurealt konteineri käivitamine (Windows'i/Mac'i käsurida on analoogiline):
```commandline
docker run -p 7000:7000 tilluteenused/vabamorf_morf
```
Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati. 

## Kasutusnäide
```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad", "annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees"}},{"start":5,"end":10,"features":{"token":"peeti"}},{"start":11,"end":16,"features":{"token":"kinni"}},{"start":16,"end":17,"features":{"token":"."}},{"start":18,"end":24,"features":{"token":"Sarved"}},{"start":24,"end":25,"features":{"token":"&"}},{"start":25,"end":30,"features":{"token":"S\u00f5rad"}}]}}' localhost:7000/process

HTTP/1.1 200 OK
Server: gunicorn
Date: Sat, 05 Feb 2022 09:59:46 GMT
Connection: close
Content-Type: application/json
Content-Length: 1418

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees","morph":[{"lemma":"Mee+s","pos":"H","feature":"sg in"},{"lemma":"Mees+0","pos":"H","feature":"sg n"},{"lemma":"Mesi+s","pos":"H","feature":"sg in"},{"lemma":"mees+0","pos":"S","feature":"sg n"},{"lemma":"mesi+s","pos":"S","feature":"sg in"}]}},{"start":5,"end":10,"features":{"token":"peeti","morph":[{"lemma":"peet+0","pos":"S","feature":"adt"},{"lemma":"pida+ti","pos":"V","feature":"ti"},{"lemma":"peet+0","pos":"S","feature":"sg p"}]}},{"start":11,"end":16,"features":{"token":"kinni","morph":[{"lemma":"kinni+0","pos":"D","feature":""}]}},{"start":16,"end":17,"features":{"token":".","morph":[{"lemma":".","pos":"Z","feature":""}]}},{"start":18,"end":24,"features":{"token":"Sarved","morph":[{"lemma":"Sarv+d","pos":"H","feature":"pl n"},{"lemma":"Sarve+d","pos":"H","feature":"pl n"},{"lemma":"Sarved+0","pos":"H","feature":"sg n"},{"lemma":"sarv+d","pos":"S","feature":"pl n"}]}},{"start":24,"end":25,"features":{"token":"&","morph":[{"lemma":"&+0","pos":"J","feature":""}]}},{"start":25,"end":30,"features":{"token":"S\u00f5rad","morph":[{"lemma":"S\u00f5ra+d","pos":"H","feature":"pl n"},{"lemma":"S\u00f5rad+0","pos":"H","feature":"sg n"},{"lemma":"S\u00f5rg+d","pos":"H","feature":"pl n"},{"lemma":"s\u00f5rg+d","pos":"S","feature":"pl n"}]}}]}}}
```
Tasub tähele panna, et Python'i json'i teek esitab teksti vaikimisi ASCII kooditabelis; 
täpitähed jms esitatakse Unicode'i koodidena, nt. õ = \u00f5.

## Päringu json-kuju
```json
{
  "type": "text",
  "content": string, /* "The text of the request" */
  "annotations": {
    "sentence": [        /* array of sentences */
      {
        "start": 0,      /* beginning of sentence */
        "end": 17        /* end of sentence */
      }
    ],
    "token": [           /* all tokens across all sentences */
      {
        "start": number, /* beginning of token */
        "end": number,   /* end of token */
        "features": {
          "token": string /* token */
        }
      }
    ]
  }
}
```
Päringu sisendiks on lausestatud ja sõnestatud tekst json-formaadis.
Selle tegemiseks saab kasutada [sõnestamise-konteinerit](https://gitlab.com/tarmo.vaino/docker-elg-tokenizer).

## Vastuse json-kuju
```json
{
  "response":
  {
    "type":"annotations",
    "annotations": {
      "sentence": [        /* array of sentences */
        {
          "start": 0,      /* beginning of sentence */
          "end": 17        /* end of sentence */
        }
      ],
      "token": [           /* all tokens across all sentences */
        {
          "start": number, /* beginning of token */
          "end": number,   /* end of token */
          "features": {
            "token": string /* token */
            "morph": [
              {
                "lemma":string,    /* lemma */
                "pos":string,      /* part of speech */
                "features":string  /* case etc */
              }
            ]            
          }
        }
      ]
    }
  }
}
```
Väljundis kasutatakse [Filosofti tähistust](https://filosoft.ee/html_morf_et/morfoutinfo.html).
*NB! Variandid POLE usutavuse järjekorras!* 

## Autorid
Tarmo Vaino, Heiki-Jaan Kaalep
## Litsents
Copyright (c) 2021 University of Tartu and Author(s).
Software is licensed under LGPL.
